﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace CityInfo.API.Controllers
{
    [Route("api/cities")]
    public class CitiesController : Controller
    {
        [HttpGet]
        // public JsonResult GetCities()
        public IActionResult GetCities()
        {
            //return new JsonResult(new List<object>() {
            //    new { Id=1, Name="New York City" },
            //    new { Id=2, Name="Honolulu" },
            //});

            // return new JsonResult(CitiesDataStore.Current.Cities);

            return Ok(CitiesDataStore.Current.Cities);
        }

        [HttpGet("{id}")]
        public IActionResult GetCity(int id)
        {
            var cityToReturn = CitiesDataStore.Current.Cities.FirstOrDefault(c => c.Id == id);
            if (cityToReturn == null)
            {
                return NotFound();
            }

            return Ok(cityToReturn);
        }

    }
}
