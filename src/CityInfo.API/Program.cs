﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace CityInfo.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args)
                .Build()
                // Runs the web application and blocks the calling thread until the web host is started.
                .Run();
        }

        // This is asp.net core 2
        // Do not remove the method, it is needed by EF Core by convention for migrations.
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
        
        // This is asp.net core 1
        public static IWebHostBuilder CreateWebHostBuilderAspNetCoreV1(string[] args)
        {
            var host = new WebHostBuilder()
                // Kestrel - a cross platform web server
                // Can directly serve, but IIS can filter request, manage certs, restart apps, etc.
                .UseKestrel() 
                // Content root bBy default it is the app base path.
                // Note: content root is different from webroot, webroot is serverd from {content-root}/wwwroot.
                .UseContentRoot(System.IO.Directory.GetCurrentDirectory())
                // In visual studio IIS Express serves as a reverse proxy for Kestrel.
                // When deploying on Windows use IIS as a reverse proxy.
                // When deploying on Linux use Apache etc.
                .UseIISIntegration() 
                // Startup class.
                .UseStartup<Startup>();
            return host;
        }
    }
}
