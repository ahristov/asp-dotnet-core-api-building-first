# Building Your First API with ASP.NET Core

Contains code and notes from studying [Building Your First API with ASP.NET Core](https://app.pluralsight.com/library/courses/asp-dotnet-core-api-building-first/table-of-contents) by Kevin Dockx

## Run

To run the app:

```bash
cd src
dotnet build
dotnet run -p CityInfo.API
```

Visit: http://localhost:5000/api/cities/  to verify.

## Notes

### Request Pipeline & Middleware

In the classic ASP.NET we had modules and handlers. The middleware in ASP.NET Core handles both needs.

Example middleware: diagnostics middleware, authentication middleware, etc. And even the MVC is a middleware that can be added to the pipeline.

Sequence:

```bash
[Request]->[Middleware1]->[Middleware2]->[Middleware1]->[Response]
```

The order in which we add the middleware modules matters. Each middleware module decides whether or not to pass the request to the next middleware on the pipeline.

If authorization failed, the authentication middleware can decide to return `401: Unauthorized` response back.

### Environments

Three values are used to describe environment by convention:

- Development
- Staging
- Production

Settings from the project properties in Visual Studio:

![environment project settings](./docs/img/2019-10-05_10h25_59.png)

Environment is not the same as a build. You can build "Debug" build to a staging environment. The environment settings are provided by the `IHostingEnvironment` service.

### ASP.NET Core MVC

The "ASP.NET Core MVC" is now one framework that _replaces_ both "ASP.NET WEb API" and "ASP.NET MVC".

Parts:

- Model: No logic, only data
- View: Only the presentational html
- Controller: Handles routes, user input, etc.

In the API world, the view is JSON.

### Wire up MVC middleware

Into the `Startup` class do the following two steps:

- Add service via extension method:

```csharp
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddMvc();
    }
```

- Wire up as middleware

We can also comment the `app.Run()`. The MVC middleware will handle HTTP requests and will handle HTTP exceptions as well:

```csharp
    public void Configure(IApplicationBuilder app, IHostingEnvironment env,
        ILoggerFactory loggerFactory)
    {
        ...
        app.UseMvc();
        //app.Run(async (context) =>
        //{
        //    await context.Response.WriteAsync("Hello World!");
        //});
```

Note: In ASP.NET Core 1 we also need to add reference to Nuget package `Microsoft.AspNetCore.Mvc`. This is not needed in ASP.NET Core 2.

ASP.NET MVC 2 is built of many small assemblies, rather than one assembly. They added `Microsoft.AspNetCore.All` meta-package. It adds references to list of packages. The list includes:

- All supported ASP.NET Core packages
- All supported Entity Framework Core packages
- Internal and 3rd-party dependencies used by ASP.NET Core and Entity Framework Core, incl. Intellisense support for Visual Studio

See [MVC Nuget Package](https://www.nuget.org/packages/Microsoft.AspNet.Mvc/) for the list of all dependencies.

How did these packages come to the project? The V2 introduced the concept of `Runtime Store`. Once the framework is installed, it copies all the _dependencies into a folder_.

You can find the files under:

```bash
C:\Program Files\dotnet\store\x64\netcoreapp2.0
```

Advantages:

- Faster deployment
- Lower disk space usage
- Faster startup, as the packages are pre-compiled

Disadvantages:

- Not self-contained
- When deploying, these assemblies are not part of the published distribution
- The runtime store needs these assemblies installed on the target machine

We still can use he old approach, but then we need to add one-by-one references to all dependency packages.

### Convention-based Routing

Default  route matches the following configuration:

```csharp
    app.UseMvc(config =>
    {
        config.MapRoute(
            name: "Default",
            template: "{controller}/{action}/{id?}",
            defaults: new { controller = "Home", action = "Index" }
        );
    });
```

This is typically used for web apps and it is not recommended for API apps.

### Attribute-based routing

Uses attributes at controller & action level, including an (optional) template.

Route http methods:

| HTTP Method | Attribute  | Level | Sample URI | Notes |
| ----------- | ---------- | ----- | ---------- | ----- |
| GET         | HttpGet    | Action  | /api/cities <br/> /api/cities/1 | Get all or some |
| POST        | HttpPost   | Action  | /api/cities     | Create new resource    |
| PUT         | HttpPut    | Action  | /api/cities/1   | Update all properties  |
| PATCH       | HttpPatch  | Action  | /api/cities/1   | Update some properties |
| DELETE      | HttpDelete | Action  | /api/cities/1   | Delete resource        |

Examples:

```csharp
    [Route("api/cities")]
    public class CitiesController : Controller
    {
        [HttpGet]
        public JsonResult GetCities()
        {
            //return new JsonResult(new List<object>() {
            //    new { Id=1, Name="New York City" },
            //    new { Id=2, Name="Honolulu" },
            //});

            return new JsonResult(CitiesDataStore.Current.Cities);
        }

        [HttpGet("{id}")]
        public JsonResult GetCity(int id)
...
```

### HTTP Response Status

By default, the ASP.NET Core handles the responses:

- 200 OK
- 404 Not Found
- 500 Internal Server Error

Levels of Status Codes:

- Level 200 Success
  - 200 OK
  - 201 Created
  - 204 No Content
- Level 400 Client Error
  - 400 Bad Request
  - 401 Unauthorized
  - 403 Forbidden
  - 404 Not Found
  - 409 Conflict
- Level 500 Server Error
  - 500 Internal Server Error

### Returning specific HTTP Response Status

We could set `JsonResult.StatusCode`, that comes from parent class `ActionResult`.

```csharp
  var res = new JsonResult(...);
  res.StatusCode = 404;
  return res;
```

Instead of returning `JsonResult`, we change to `IActionResult`. One benefit of using `IActionResult` is that, the consumer may decide what format the data will be in.

Now we can use the shortcut methods that the framework provides:

```csharp
        [HttpGet("{id}")]
        public IActionResult GetCity(int id)
        {
            var cityToReturn = CitiesDataStore.Current.Cities.FirstOrDefault(c => c.Id == id);
            if (cityToReturn == null)
            {
                return NotFound();
            }

            return Ok(cityToReturn);
        }
```

### Status Code Pages

If we open in the browser URL for city that does not exist `https://localhost:44365/api/cities/ 5`, we see empty page:

![404 Not found in browser](./docs/img/2019-10-06_11h47_16.png)

```text
HTTP/1.1 404 Not Found
Server: Kestrel
X-Powered-By: ASP.NET
Date: Sun, 06 Oct 2019 16:52:02 GMT
Connection: close
Content-Length: 0

```

We can add in `Startup.cs`:

```csharp
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            ...
            app.UseStatusCodePages();
            ...
```

, and then we get:

![404 Not found in browser](./docs/img/2019-10-06_11h54_08.png)

```text
HTTP/1.1 404 Not Found
Transfer-Encoding: chunked
Content-Type: text/plain
Server: Kestrel
X-Powered-By: ASP.NET
Date: Sun, 06 Oct 2019 16:52:58 GMT

20f
Status Code: 404; Not Found
0
```

### Returning Child Resources

See `PointsOfInterestController`. Note we are returning `404 Not found` when either city or point of interest not found.

### JSON Serializer formatting

Bu default `Newtonsoft.Json` uses _camelCase_ format for property names.

To make the Json property names to be named same as defined property names in the class, use `AddJsonOptions` in the method `ConfigureServices` inside the `Startup` class:

```csharp
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddMvc()
            // Override JSON property naming:
            .AddJsonOptions(o =>
            {
                if (o.SerializerSettings.ContractResolver != null)
                {
                    var castedResolver = o.SerializerSettings.ContractResolver
                        as Newtonsoft.Json.Serialization.DefaultContractResolver;
                    castedResolver.NamingStrategy = null; // Json property names will be named same as defined property names in the class.
                }
            })
        ;
    }

```

### Formatters and Content Negotiation

The client may request different formats via the _Accept_ header:

- application/json
- application/xml
- ...

The framework supports different response formats via the _output formatter_. It responds to the media type of the `Accept` header.

Note: similarly there is the concept of the _input formatter_. It deals with the media type of the `Content-Type` header.

Our cities API currently only returns JSON (this is by default). We add XML serializer format into `ConfigireServices` in the `Startup` class, via the `AddMvcOptions`:

```csharp
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                // Add xml output serializer
                .AddMvcOptions(o => o.OutputFormatters.Add(
                    new XmlDataContractSerializerOutputFormatter()
                ))
...
```

```xml
GET https://localhost:44365/api/cities/ HTTP/1.1
Accept: application/xml
...

HTTP/1.1 200 OK
Transfer-Encoding: chunked
Content-Type: application/xml; charset=utf-8
..

467
<?xml version="1.0" encoding="utf-8"?>
<ArrayOfCityDto xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.datacontract.org/2004/07/CityInfo.API.Models">
  <CityDto>
    <Description>The one with that big park.</Description>
    <Id>1</Id>
    <Name>New York City</Name>
    <PointsOfInterest>
      <PointOfInterestDto>
        <Description>The most visited urban park in the United States.</Description>
        <Id>1</Id>
        <Name>Central Park</Name>
      </PointOfInterestDto>
...
```

### NLog

Inject `ILogger` to the controller:

```sharp
        readonly ILogger _logger;

        public PointsOfInterestController(ILogger logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
```

Add logging to the logger:

```
            if (city == null)
            {
                _logger.LogInformation($"City with id {cityId} was not found when accessing points of interests");
                return NotFound();
            }
```

Add Nuget package `NLog.Extensions.Logging` to the project:

```bash
dotnet add package NLog.Extensions.Logging --version 1.6.1
```

Add configuration file `nlog.config`:

```xml
<?xml version="1.0" encoding="utf-8" ?>
<nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd" 
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <targets>
    <target name="logfile" xsi:type="File" fileName="nlog-${shortdate}.log" />
  </targets>

  <rules>
    <logger name="*" minlevel="Info" writeTo="logfile" />
  </rules>
</nlog>
```

Wire up in `Startup` class:

```csharp
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();
            loggerFactory.AddDebug();
            loggerFactory.AddProvider(new NLog.Extensions.Logging.NLogLoggerProvider());

```

But there is an easier way, a shortcut extension methods is provided, but we wire it up in  `ConfigureServices`:

```csharp
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddConsole();
                loggingBuilder.AddDebug();
                loggingBuilder.AddNLog();
            });
```

### Add Service

Suppose we have email sending service:

```csharp
using System.Diagnostics;

namespace CityInfo.API.Services
{
    public class LocalMailService
    {
        private string _mailTo = "admin@mycompany.com";
        private string _mailFrom = "noreply@mycompany.com";

        public void Send(string subject, string message)
        {
            // send mail - output to debug window
            Debug.WriteLine($"Mail from {_mailFrom} to {_mailTo}, with {nameof(LocalMailService)}");
            Debug.WriteLine($"Subject: {subject}");
            Debug.WriteLine($"Message: {message}");
        }
    }
}
```

We register the service in `Startup.ConfigureServices()`:

```csharp
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<LocalMailService>();
```

They are **three ways to register** the service and they refer to the life-time of the service instance:

- `AddTransient()`: An instance is created each time it is requested.

- `AddScoped()`: An instance is created once per request.

- `AddSingleton():` An instance is created at the first time it is requested, or you can pass in an instance to it.

And then we inject in the controller:

```csharp
namespace CityInfo.API.Controllers
{
    [Route("api/cities")]
    public class PointsOfInterestController : Controller
    {
        readonly ILogger _logger;
        readonly LocalMailService _mailService;

        public PointsOfInterestController(
            ILogger<PointsOfInterestController> logger,
            LocalMailService mailService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mailService = mailService ?? throw new ArgumentNullException(nameof(mailService));
        }
...
```

### Dependency Injection with Interface

We can wire up DI to interface instead of class. Example:

```csharp
        public void ConfigureServices(IServiceCollection services)
        {
            // services.AddTransient<LocalMailService>();
#if DEBUG
            services.AddTransient<IMailService, LocalMailService>();
#else
            services.AddTransient<IMailService, CloudMailService>();
#endif
```

### Configuration

Add configuration file `appSettings.json`:

```json
{
  "mailSettings": {
    "mailToAddress": "admin@mycompany.com",
    "mailFromAddress": "noreply@mycompany.com"
  }
}
```

#### Configuration Setup in ASP.Net Core 1

Build and expose as static field in `Startup`:

```csharp
    public class Startup
    {
        public static IConfigurationRoot Configuration;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appSettings.json", optional: false, reloadOnChange: true);

            Configuration = builder.Build();
        }
```

Then in the service classes we can access as:

```csharp
    public class LocalMailService : IMailService
    {
        private string _mailTo = Startup.Configuration["mailSettings:mailToAddress"];
        private string _mailFrom = Startup.Configuration["mailSettings:mailFromAddress"];
```

#### Configuration Setup in ASP.Net Core 2

We are using more generic `IConfiguration` object:

```csharp
    public class Startup
    {
        public static IConfiguration Configuration { get; private set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }
```

### Configuration Scoping

We have main configuration file, and we can add per environment config files, that only override whatever is different:

appSettings.json:

```json
{
  "mailSettings": {
    "mailToAddress": "developer@mycompany.com",
    "mailFromAddress": "noreply@mycompany.com"
  }
}
```

appSettings.Production.json:

```json
{
  "mailSettings": {
    "mailToAddress": "admin@mycompany.com"
  }
}
```

In ASP.Net Core 1 we need to add one more call to `AddJsonFile()`:

```csharp
    public class Startup
    {
        public static IConfigurationRoot Configuration;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appSettings.json", optional: false, reloadOnChange: true);
                .AddJsonFile($"appSettings.{env.EnvironmentName}.json", optional: false, reloadOnChange: true);

            Configuration = builder.Build();
        }

```

In ASP.Net Core 2 no changes are needed.

## Links

Use links:

[ASP.NET Core Logging](https://github.com/aspnet/Logging)  
[NLog](https://nlog-project.org/)  
